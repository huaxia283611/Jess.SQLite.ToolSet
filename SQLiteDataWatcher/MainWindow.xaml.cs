﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SQLiteDataWatcher
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "SQLite DBFiles|*.db|ALL Files|*.*";
            if (ofd.ShowDialog() ?? false)
            {
                this.tb_dbpath.Text = ofd.FileName;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            SQLiteConnection cn = new SQLiteConnection("Data Source=" + this.tb_dbpath.Text + ";Password=" + this.tb_pwd.Text + ";Version=3;");
            try
            {
                cn.Open();
                string sql = string.Format("select * from {0};", this.tb_watchtable.Text);
                SQLiteCommand cmd = new SQLiteCommand();
                cmd.CommandText = sql;
                cmd.Connection = cn;
                SQLiteDataAdapter da = new SQLiteDataAdapter();
                da.SelectCommand = cmd;
                DataTable dt = new DataTable();
                da.Fill(dt);
                this.dg_tabledata.DataContext = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        private void treeView_DragEnter(object sender, DragEventArgs e)
        {
            //e.Effects
        }

        private void treeView_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void treeView_Drop(object sender, DragEventArgs e)
        {
            //e.Data.GetData(DataFormats.FileDrop)
        }
    }
}
