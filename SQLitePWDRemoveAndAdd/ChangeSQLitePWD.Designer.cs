﻿namespace SQLitePWDRemoveAndAdd
{
    partial class ChangeSQLitePWD
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangeSQLitePWD));
            this.tb_sqliteDBpath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_BrowserPath = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_sqlitedbPWD = new System.Windows.Forms.TextBox();
            this.btn_AddPWD = new System.Windows.Forms.Button();
            this.btn_RemovePWD = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_ChangeLanguage = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tb_sqliteDBpath
            // 
            this.tb_sqliteDBpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.tb_sqliteDBpath, "tb_sqliteDBpath");
            this.tb_sqliteDBpath.Name = "tb_sqliteDBpath";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // btn_BrowserPath
            // 
            resources.ApplyResources(this.btn_BrowserPath, "btn_BrowserPath");
            this.btn_BrowserPath.Name = "btn_BrowserPath";
            this.btn_BrowserPath.UseVisualStyleBackColor = true;
            this.btn_BrowserPath.Click += new System.EventHandler(this.btn_BrowserPath_Click);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // tb_sqlitedbPWD
            // 
            this.tb_sqlitedbPWD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.tb_sqlitedbPWD, "tb_sqlitedbPWD");
            this.tb_sqlitedbPWD.Name = "tb_sqlitedbPWD";
            // 
            // btn_AddPWD
            // 
            resources.ApplyResources(this.btn_AddPWD, "btn_AddPWD");
            this.btn_AddPWD.Name = "btn_AddPWD";
            this.btn_AddPWD.UseVisualStyleBackColor = true;
            this.btn_AddPWD.Click += new System.EventHandler(this.btn_AddPWD_Click);
            // 
            // btn_RemovePWD
            // 
            resources.ApplyResources(this.btn_RemovePWD, "btn_RemovePWD");
            this.btn_RemovePWD.Name = "btn_RemovePWD";
            this.btn_RemovePWD.UseVisualStyleBackColor = true;
            this.btn_RemovePWD.Click += new System.EventHandler(this.btn_RemovePWD_Click);
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btn_RemovePWD, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.btn_AddPWD, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.tb_sqliteDBpath, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.btn_BrowserPath, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.tb_sqlitedbPWD, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.btn_ChangeLanguage, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 1, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // btn_ChangeLanguage
            // 
            resources.ApplyResources(this.btn_ChangeLanguage, "btn_ChangeLanguage");
            this.btn_ChangeLanguage.Name = "btn_ChangeLanguage";
            this.btn_ChangeLanguage.UseVisualStyleBackColor = true;
            this.btn_ChangeLanguage.Click += new System.EventHandler(this.btn_ChangeLanguage_Click);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // ChangeSQLitePWD
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ChangeSQLitePWD";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tb_sqliteDBpath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_BrowserPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_sqlitedbPWD;
        private System.Windows.Forms.Button btn_AddPWD;
        private System.Windows.Forms.Button btn_RemovePWD;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btn_ChangeLanguage;
        private System.Windows.Forms.Label label3;
    }
}

