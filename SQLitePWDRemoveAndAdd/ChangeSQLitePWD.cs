﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace SQLitePWDRemoveAndAdd
{
    /// <summary>
    /// 修改SQLite密码——添加移除
    /// </summary>
    public partial class ChangeSQLitePWD : Form
    {

        public ChangeSQLitePWD()
        {
            InitializeComponent();
        }

        #region 功能性事件
        /// <summary>
        /// 浏览Sqlite数据库路径
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_BrowserPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "SQLite DBFiles|*.db|ALL Files|*.*";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                this.tb_sqliteDBpath.Text = ofd.FileName;
            }
        }

        /// <summary>
        /// 添加密码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_AddPWD_Click(object sender, EventArgs e)
        {
            SQLiteConnection cn = new SQLiteConnection("Data Source=" + this.tb_sqliteDBpath.Text + ";Version=3;");
            try
            {
                cn.Open();
                cn.ChangePassword(this.tb_sqlitedbPWD.Text.Trim());
                MessageBox.Show("密码添加成功");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        /// <summary>
        /// 密码移除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_RemovePWD_Click(object sender, EventArgs e)
        {
            SQLiteConnection cn = new SQLiteConnection("Data Source=" + this.tb_sqliteDBpath.Text + ";Version=3;");
            try
            {
                cn.SetPassword(this.tb_sqlitedbPWD.Text.Trim());
                cn.Open();
                cn.ChangePassword("");
                MessageBox.Show("移除密码成功！");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }
        #endregion

        /// <summary>
        /// 语言切换
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_ChangeLanguage_Click(object sender, EventArgs e)
        {

            switch (this.btn_ChangeLanguage.Text.Trim())
            {
                case "中文":
                    System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("zh-CN");
                    break;
                default:
                    System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-us");
                    break;
            }

            ApplyResource();
        }

        #region 语言切换辅助
        ComponentResourceManager resources = new ComponentResourceManager(typeof(ChangeSQLitePWD));

        private void ApplyResource()
        {
            foreach (Control ctl in this.Controls)
            {
                Apply(ctl);
            }
            this.ResumeLayout(false);
            this.PerformLayout();
            resources.ApplyResources(this, "$this");
        }

        private void Apply(Control ctl)
        {
            resources.ApplyResources(ctl, ctl.Name);
            if (ctl.HasChildren)
            {
                foreach (Control item in ctl.Controls)
                {
                    Apply(item);
                }
            }
        }

        #endregion
    }
}
