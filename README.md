# Jess.SQLite.ToolSet

#### 介绍
Sqlite 工具集

## 内容

* **SQLite密码添加移除**工具

## RoadMap

* sqlite内容查看工具——sqlite查看软件
    * 拖动打开【因为sqlite为文件数据库，最好是直接拖动打开查看】
    * or vscode插件？


## 开发推荐内容

* [Link.EntityFramework.Sqlite](https://github.com/ShiJess/Link.EntityFramework.Sqlite)
    * 个人以其他开源项目为基础改的**EF6**-SQLite库
    * 主要功能：支持在**自动迁移**时，删除列和修改列信息